package studentdatabaseapp;
import java.util.Scanner;


//coding help taken from Master Skills and my Group (Mac, Lucas, Jeff).


public class Student {
    private String firstName;
    private String lastName;
    private int gradeYear;
    private String studentID;
    private String courses;

    private int tuitionBalance = 0;
    private static int costOfCourse = 1000;

    private static int id = 1001;


    //Constructor: name and year

    public Student ()  {
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter student first name: ");
        this.firstName = in.nextLine();

        System.out.print("Please enter student last name:  ");
        this.lastName = in.nextLine();

        System.out.print("1 - Freshman\n2 - Sophmore\n3 - Junior\n4- Senior\nEnter student grade level: ");
        this.gradeYear = in.nextInt();

        setstudentID();

        //System.out.println(firstName + "  " + lastName + "  " + gradeYear + "  " + studentID);


    }

    //generate ID number (Static)

    private void setstudentID(){
        //Grade + ID
        id++;
        this.studentID = gradeYear + "" + id;
    }

    //Enroll

    public void enroll()  {
        //Loop, until user hits quit

        do {
            System.out.print("Enter course to enroll (Q to quit):  ");
            Scanner in = new Scanner(System.in);
            String course = in.nextLine();
            if(!course.equals("Q"))  {
                courses = courses + "\n " + course;
                tuitionBalance = tuitionBalance + costOfCourse;
            }
            else {
                break;
            }
        }while (1 != 0);

        //System.out.println("Enrolled In:  " + (courses));

    }

    //View Balance
    public void viewBalance() {
        System.out.println("Your Balance is:  $" + tuitionBalance);
    }

    //Pay Tuition
    public void payTuition() {
        viewBalance();
        System.out.print("Enter your payment amount: $");
        Scanner in = new Scanner(System.in);
        int payment = in.nextInt();
        tuitionBalance = tuitionBalance - payment;
        System.out.println("Thank you for your payment of $" + payment);
        viewBalance();
    }

    //Show status

    public String toString()  {
        return  "Name: " + firstName + "  " + lastName +
                "\nGrade Level: " + gradeYear +
                "\nStudent ID: " + studentID +
                "\nCourses Enrolled:" + courses
                + "\nBalance: $" +tuitionBalance;
    }
}

