import java.util.*;
//coding help taken from geeksforgeeks.org
class Queue {
    public static void main(String args[])
    {
        // Creating empty priority queue
        PriorityQueue<Integer> pQueue
                = new PriorityQueue<Integer>();

        // Adding items to the pQueue using add()
        pQueue.add(10);
        pQueue.add(20);
        pQueue.add(15);

        for (Integer element : pQueue)
            System.out.print(element + " ");

        System.out.println();
    }
}
