import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
//coding help taken from ttuckett and MacAllen

public class Run {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(4);

        Do d1 = new Do("Amanda", 20, 100);
        Do d2 = new Do("Jeff", 15, 50);
        Do d3 = new Do("Dave", 10, 25);
        Do d4 = new Do("Mac", 5, 10);
        Do d5 = new Do("Allen", 2, 15);
        Do d6 = new Do("Alison", 1, 10);

        myService.execute(d1);
        myService.execute(d2);
        myService.execute(d3);
        myService.execute(d4);
        myService.execute(d5);
        myService.execute(d6);

        myService.shutdown();
        while(!myService.isTerminated()) {

        }
        System.out.println("Finished all threads");
    }
}
