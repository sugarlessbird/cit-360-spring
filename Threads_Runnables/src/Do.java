import java.util.Random;

//coding help taken from ttuckett and MacAllen

public class Do implements Runnable {

        private String name;
        private int number;
        private int walk;
        private int rand;

        public Do(String name, int number, int walk) {

            this.name = name;
            this.number = number;
            this.walk = walk;

            Random random = new Random();
            this.rand = random.nextInt(100);
        }

        public void run() {
            System.out.println("\n\nThese are our parameters: Name =" + name + " Number = "
                    + number + " Walk = " + walk + " Rand Num = " + rand + "\n\n");
            for (int count = 1; count < rand; count++) {
                if (count % number == 0) {
                    System.out.print(name + " is walking. ");
                    try {
                        Thread.sleep(walk);
                    } catch (InterruptedException e) {
                        System.err.println(e);
                    }
                }
            }
            System.out.println("\n\n" + name + " is done walking.\n\n");
        }
    }

