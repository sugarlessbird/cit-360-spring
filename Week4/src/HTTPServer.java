
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.lang.String;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

//coding help (LOTS of help) from Lucas and Jeff, as well as TTuckett code.
//shout out to Jeff, who helped me so much when I was having troubles with the Jackson, he really spent a lot of his Saturday helping me fix it.


public class HTTPServer {
    public HTTPServer(Fact factstring) throws IOException {
        HttpServer httpServer = HttpServer.create(new InetSocketAddress(80), 0);
        httpServer.createContext("/json", (HttpHandler) new handler(factstring));
        httpServer.setExecutor(null);
        httpServer.start();

    }

    private class handler implements HttpHandler {
        private Fact _fact;

        public handler(Fact fact) {
            _fact = fact;
        }

        public void handle(HttpExchange exchange) throws IOException {
            String response = factToJSOn(_fact);
            exchange.sendResponseHeaders(200, response.length());
            exchange.getResponseHeaders().set("Content-type", "application/json");
            OutputStream os = exchange.getResponseBody();
            os.write(response.getBytes());
            os.close();


        }
    }

    public String factToJSOn (Fact fact) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
         return mapper.writeValueAsString(fact);


    }


}

