
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
//coding help taken from Lucas and Jeff from Group 4, as well as TTuckett code.


public class Client {
    public static void main(String[] args) {
        try {
            Fact fact = new Fact();
            fact.setFact("Did you know that many foot disorders in dogs are caused by long toenails? ");


           HTTPServer server = new HTTPServer(fact);
            String json = getContent("http://localhost/json");
            System.out.println("Checking Json");
            System.out.println(json);

            System.out.println("Print out of JSON fact");
            System.out.println(JSONtoFact(json));

        } catch (IllegalArgumentException | IOException e) {
            System.out.println("hi" + e.toString());

        }

    }


    public static String getContent(String urlstring) {
        String content = "";
        try {
            URL url = new URL(urlstring);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append((line + "\n"));

            }
            content = stringBuilder.toString();
        } catch (Exception e) {
            System.err.println(e.toString());
        }
        return content;
    }

    public static Fact JSONtoFact(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Fact fact = null;
        fact = mapper.readValue(json, Fact.class);
        return fact;
    }
}
