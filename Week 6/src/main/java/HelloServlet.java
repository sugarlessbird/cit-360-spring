import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

//coding help taken from ttuckett and tutorialspoint.com

@WebServlet(name = "HelloServlet", urlPatterns={"/HelloServlet"})
public class HelloServlet extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String first_name = request.getParameter("first_name");
        String last_name = request.getParameter("last_name");
        out.println("<h1>Your Name Is</h1>");
        out.println("<p>First Name: " + first_name + "</p>");
        out.println("<p>Last Name: " + last_name + "</p>");
        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available right now, go have a Diet Coke and relax.");
    }

}

