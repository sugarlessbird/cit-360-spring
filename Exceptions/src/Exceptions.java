import java.util.Scanner;

//coding help taken from W3Resource.com & developer.com
public class Exceptions {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input the first number: ");
        int a = input.nextInt();
        System.out.print("Input the second number: ");
        try {
            int b = input.nextInt();
            int d = (a / b);
            System.out.println();
            System.out.println("The division of the first number and the second number is:" + d);

        } catch (ArithmeticException e) {
            System.out.println("You Shouldn't divide a number by zero, please try again");

        } finally {


            System.out.println(
                    "finally, division can be fun!");

        }
    }
}



